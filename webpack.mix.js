// webpack.mix.js
let mix = require('laravel-mix');

mix.js('src/js/app.js', 'js/').sass('src/scss/app.scss', 'css/').options({
   processCssUrls: false
});

mix.js('src/js/main.js', 'js/main.js');

mix.copy('src/fonts', 'fonts/');
mix.copy('src/img', 'img/');