/*mobile burger*/
const menuBtn = document.querySelector('.menu-btn');
let menuOpen = false;
menuBtn.addEventListener('click', () => {
    if(!menuOpen) {
        menuBtn.classList.add('open');
        menuOpen = true;
    } else {
        menuBtn.classList.remove('open');
        menuOpen = false;
    }
});

const menuBtnBurger = document.querySelector('.menu-btn__burger');
/*mobile burger end*/
$(function() {
    $(".show-more").click(function(){
        $(this).parents(".cup").toggleClass('show');
    });

    $(".mob-close").click(function () {
        $('.mobile-menu').css('left', '-342px').removeClass('active');
        menuBtn.classList.remove('open');
        menuOpen = false;
    });
    $(".burger .menu-btn").click(function () {
        if($('.mobile-menu').hasClass('active')){
            $('.mobile-menu').css('left', '-342px').removeClass('active');
        }else{
            $('.mobile-menu').css('left', '0').addClass('active');
        }
    });
});
